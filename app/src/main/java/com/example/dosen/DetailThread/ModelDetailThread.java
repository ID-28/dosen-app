package com.example.dosen.DetailThread;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelDetailThread {
    @SerializedName("idThread")
    @Expose
    private int idThread;

    @SerializedName("judulThread")
    @Expose
    private String judulThread;

    @SerializedName("ket")
    @Expose
    private String ket;

    @SerializedName("posted")
    @Expose
    private String posted;

    @SerializedName("namaMahasiswa")
    @Expose
    private String namaMahasiswa;

    @SerializedName("username")
    @Expose
    private String username;

    public ModelDetailThread(String judulThread,  String namaMahasiswa, String username, String posted, String ket) {
        this.judulThread = judulThread;
        this.namaMahasiswa = namaMahasiswa;
        this.username = username;
        this.posted = posted;
        this.ket = ket;
    }

    public int getIdThread() {
        return idThread;
    }

    public void setIdThread(int idThread) {
        this.idThread = idThread;
    }

    public String getJudulThread() {
        return judulThread;
    }

    public void setJudulThread(String judulThread) {
        this.judulThread = judulThread;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
