package com.example.dosen.Komen;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.example.dosen.CustomOnItemClickListener;
import com.example.dosen.R;

import java.util.ArrayList;

public class ListKomen extends ArrayAdapter <ModelKomen>{

    private ArrayList<ModelKomen> list;
    private LayoutInflater inflater;
    private int res;

    public ListKomen(@NonNull Context context, int resource, ArrayList<ModelKomen> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ListKomen.MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new ListKomen.MyHolder();

            holder.Komentator = (TextView) convertView.findViewById(R.id.nama_komentator);
            holder.Komentar = (TextView) convertView.findViewById(R.id.komentar);
            holder.Komen = (CardView) convertView.findViewById(R.id.cardview_detail_komen);

            convertView.setTag(holder);
        } else {
            holder = (ListKomen.MyHolder) convertView.getTag();
        }

        holder.Komentator.setText(list.get(position).getNama() + " || " + list.get(position).getUsername() + " at " + list.get(position).getCreated_at());
        holder.Komentar.setText("Commented : \n" + list.get(position).getComment());

        if (list.get(position).getIdComment() == list.get(position).getStatus()) {
            holder.Komen.setCardBackgroundColor(Color.GREEN);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelKomen object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {

        //TextView ID;
        TextView JudulThread;
        TextView Komentator;
        TextView Komentar;

        CardView Komen;
    }
}
