package com.example.dosen.Materi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.dosen.CustomOnItemClickListener;
import com.example.dosen.R;
import com.example.dosen.ThreadList.ThreadList;

import java.util.ArrayList;

public class ListMateri extends ArrayAdapter <ModelMateri> {

    private ArrayList<ModelMateri> list;
    private LayoutInflater inflater;
    private int res;
    ProgressDialog loading;

    public ListMateri(@NonNull Context context, int resource, ArrayList<ModelMateri> list){
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ListMateri.MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new ListMateri.MyHolder();

            holder.Name = (TextView) convertView.findViewById(R.id.nama_materi);
            holder.Materi = (CardView) convertView.findViewById(R.id.cardview_materi);

            convertView.setTag(holder);
        } else {
            holder = (ListMateri.MyHolder) convertView.getTag();
        }

        holder.Name.setText(list.get(position).getNamaMateri());
        holder.Materi.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, ThreadList.class);
                intent.putExtra("id_materi", String.valueOf(list.get(position).getIdMateri()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModelMateri object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView Name;
        CardView Materi;
    }
}
