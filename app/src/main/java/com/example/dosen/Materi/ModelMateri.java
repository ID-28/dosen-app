package com.example.dosen.Materi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMateri {
    @SerializedName("namaMateri")
    @Expose
    private String namaMateri;

    @SerializedName("idMateri")
    @Expose
    private int idMateri;

    public ModelMateri(String namaMateri, int idMateri) {
        this.namaMateri = namaMateri;
        this.idMateri = idMateri;
    }


    public void setNamaMateri(String namaMateri){
        this.namaMateri = namaMateri;
    }

    public String getNamaMateri(){
        return namaMateri;
    }

    public int getIdMateri() {
        return idMateri;
    }

    public void setIdMateri(int idMateri) {
        this.idMateri = idMateri;
    }
}
