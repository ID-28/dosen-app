package com.example.dosen;

import com.example.dosen.DetailThread.ModelDetailThread;
import com.example.dosen.Komen.ModelKomen;
import com.example.dosen.Materi.ModelMateri;
import com.example.dosen.Modul.ModelModul;
import com.example.dosen.Praktikum.ModelPraktikum;
import com.example.dosen.ThreadList.ModelListThread;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @FormUrlEncoded
    @POST("login-dosen")
    Call<ModelUser> getLoginData(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("dosen/thread")
    Call<List<ModelPraktikum>> getPraktikum(@Field("username") String username);

    @GET("dosen/thread/{praktikum}")
    Call<List<ModelModul>> getModul(@Path("praktikum") String praktikum);

    @GET("dosen/thread/modul/{modul}")
    Call<List<ModelMateri>> getMateri(@Path("modul") String modul);

    @GET("dosen/thread/materi/{materi}")
    Call<List<ModelListThread>> getThreadList(@Path("materi") String materi);

    @GET("dosen/thread/detail/{thread}")
    Call<ModelDetailThread> getThreadDetail(@Path("thread") String thread);

    @GET("dosen/thread/comment/praktikan/{thread}")
    Call<List<ModelKomen>> getKomenPraktikan(@Path("thread") String thread);

    @GET("dosen/thread/comment/dosen/{thread}")
    Call<List<ModelKomen>> getKomenDosen(@Path("thread") String thread);

    @GET("dosen/thread/comment/aslab/{thread}")
    Call<List<ModelKomen>> getKomenAslab(@Path("thread") String thread);

    @FormUrlEncoded
    @POST("dosen/comment/create/{thread}")
    Call<ModelKomen> addComent (@Field("comment") String comment, @Field("user_id") String user_id, @Path("thread") String thread);


}
