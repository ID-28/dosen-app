package com.example.dosen.Praktikum;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.dosen.ApiService;
import com.example.dosen.MainActivity;
import com.example.dosen.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Praktikum extends AppCompatActivity {
    ArrayList<ModelPraktikum> data_praktikum = new ArrayList<ModelPraktikum>();
    ListView listview;
    ListPraktikum adapter;

    TextView PKosong;

    ProgressDialog loading;
    String sp_username;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.praktikum);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        sp_username = sharedPreferences.getString("kirim_username", "");

        PKosong = (TextView) findViewById(R.id.praktikum_kosong);

        listview = (ListView) findViewById(R.id.list_praktikum);
        listview.setDividerHeight(0);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(Praktikum.this, null, "Please wait...", true, false);
        Call<List<ModelPraktikum>> call = service.getPraktikum(sp_username);
        call.enqueue(new Callback<List<ModelPraktikum>>() {
            @Override
            public void onResponse(Call<List<ModelPraktikum>> call, Response<List<ModelPraktikum>> response) {
                data_praktikum.clear();
                if (response.isSuccessful()) {
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModelPraktikum data = new ModelPraktikum(
                                response.body().get(i).getNamaPraktikum(),
                                response.body().get(i).getidPraktikum()
                        );

                        data_praktikum.add(data);
                    }

                    adapter = new ListPraktikum(Praktikum.this, R.layout.item_praktikum, data_praktikum);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ) {
                        listview.setVisibility(View.INVISIBLE);
                        PKosong.setVisibility(View.VISIBLE);
                    }
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModelPraktikum>> call, Throwable t) {
                Toast.makeText(Praktikum.this, "Error Retrive Data from Server!!!\n" + t.getMessage(),Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 ){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_praktikum.clear();
        adapter = new ListPraktikum(Praktikum.this, R.layout.item_praktikum, data_praktikum);
        listview.setAdapter(adapter);
        setup();
    }
}
